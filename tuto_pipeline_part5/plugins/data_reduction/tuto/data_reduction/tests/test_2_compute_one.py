import pytest
from unittest import mock
from random import sample, randrange
from argparse import Namespace

from outflow.core.test import TaskTestCase
from outflow.core.pipeline import context, config

from tuto.data_reduction.tasks import ComputeOne
from tuto.data_reduction.models.computation_result import ComputationResult

class TestDataReductionComputeTasks(TaskTestCase):

    @pytest.fixture(autouse=True)
    def setup_context(self, with_pipeline_context_manager):
        context.force_dry_run = False
        context.db_untracked = False
        context._models = []
        config["databases"] = mock.MagicMock()

    @mock.patch('outflow.core.db.database.Databases.session', new_callable=mock.PropertyMock)
    def test_compute_one_already_in_db(self, mock_session):

        # --- define test data ---
        data = randrange(100)
        mult = randrange(10)
        db_result = ComputationResult(
            input_value = data,
            multiplier = mult,
            result = data * mult
        )
        
        # --- define mock session queries
        mock_session\
            .return_value.query\
            .return_value.filter_by\
            .return_value.one.return_value = db_result

        # -- define args
        context.args = Namespace(multiplier = mult, dry_run = False, db_untracked = False)
        self.task = ComputeOne()

        # --- run the task ---
        result = self.run_task(some_data = data)

        # filtering "add" calls with a ComputationResult object as parameter
        call_add_computation_result = [
            call.args[0] 
            for call in mock_session.return_value.add.call_args_list 
            if isinstance(call.args[0], ComputationResult)]

        # --- make assertions ---
        assert isinstance(result, dict)
        assert 'computation_result' in result
        assert result == {'computation_result': data * mult}
        assert len(call_add_computation_result) == 0
        
    # @mock.patch('tuto.data_reduction.tasks.context')
    @mock.patch('outflow.core.db.database.Databases.session', new_callable=mock.PropertyMock)
    def test_compute_one_not_in_db(self, mock_session):

        # --- initialize the task ---
        from sqlalchemy.orm.exc import NoResultFound

        # --- define test data ---
        data = randrange(100)
        mult = randrange(10)
        db_result = ComputationResult(
            input_value = data,
            multiplier = mult,
            result = data * mult
        )

        # --- define mock session queries
        mock_session\
            .return_value.query\
            .return_value.filter_by\
            .return_value.one.side_effect = NoResultFound

        # -- define args
        context.args = Namespace(multiplier = mult, dry_run = False, db_untracked = False)
        self.task = ComputeOne()

        # --- run the task ---
        result = self.run_task(some_data = data)

        # filtering "add" calls with a ComputationResult object as parameter
        call_add_computation_result = [
            call.args[0] 
            for call in mock_session.return_value.add.call_args_list 
            if isinstance(call.args[0], ComputationResult)]

        # --- make assertions ---
        assert isinstance(result, dict)
        assert 'computation_result' in result
        assert result == {'computation_result': data * mult}
        assert len(call_add_computation_result) == 1
        