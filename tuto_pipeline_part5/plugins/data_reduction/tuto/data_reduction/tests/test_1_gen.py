from outflow.core.test import TaskTestCase


class TestDataReductionGenTasks(TaskTestCase):
    def test_gen_one(self):
        # --- initialize the task ---
        from tuto.data_reduction.tasks import GenOneData

        self.task = GenOneData()
        self.config = {}

        # --- run the task ---
        result = self.run_task()

        # --- make assertions ---
        # test the result
        assert isinstance(result, dict)
        assert 'some_data' in result
        assert result == {'some_data': 40}

    def test_gen_more(self):
        # --- initialize the task ---
        from tuto.data_reduction.tasks import GenMoreData

        self.task = GenMoreData()
        self.config = {}

        # --- run the task ---
        result = self.run_task()

        # --- make assertions ---
        # test the result
        assert isinstance(result, dict)
        assert 'some_data_array' in result
        assert result == {'some_data_array': [40, 50, 60, 70]}

