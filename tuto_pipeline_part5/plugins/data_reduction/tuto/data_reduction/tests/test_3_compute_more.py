import pytest
from random import sample, randrange
from argparse import Namespace
from unittest import mock

from outflow.core.test import TaskTestCase
from outflow.library.tasks import MapTask

from outflow.core.pipeline import context, config

from tuto.data_reduction.models.computation_result import ComputationResult
from tuto.data_reduction.tasks import ComputeMore

class TestDataReductionComputeMoreTasks(TaskTestCase):

    @pytest.fixture(autouse=True)
    def setup_context(self, with_pipeline_context_manager):
        context.force_dry_run = False
        context.db_untracked = False
        context._models = []
        config["databases"] = mock.MagicMock()
        config["backend"] = "default"
        context.backend_name = "default"

    @mock.patch('outflow.core.db.database.Databases.session', new_callable=mock.PropertyMock)
    def test_compute_more(self, mock_session):
        """
        Testing when all data are already in database
        Request to DB returns a ComputationResult 
        """

        # --- define test data ---
        nb_data = randrange(1,5)
        data_array = sample(range(100), k=nb_data)
        mult = randrange(10)

        # --- define the data returned by the request to the DB ---
        db_result_list = [ComputationResult(
            input_value = data_array[i],
            multiplier = mult,
            result = data_array[i] * mult
        ) for i in range(nb_data) ]

        # --- define mock session queries
        mock_session\
            .return_value.query\
            .return_value.filter_by\
            .return_value.one.side_effect = db_result_list

        # -- define args
        context.args = Namespace(multiplier = mult, dry_run = False, db_untracked = False)

        # --- initialize the task ---
        with MapTask(output_name="map_computation_result") as mapped_computation:
            compute = ComputeMore()
            
        self.task = mapped_computation

        # --- run the task ---
        result = self.run_task(some_data_array = data_array)

        # --- make assertions ---

        # filtering "add" calls with a ComputationResult object as parameter
        call_add_computation_result = [
            call.args[0] 
            for call in mock_session.return_value.add.call_args_list 
            if isinstance(call.args[0], ComputationResult)]

        # all results are already be in base, since we mock a result in base
        assert len(call_add_computation_result) == 0

        # check the type of result
        assert isinstance(result, dict)
        assert 'map_computation_result' in result

        # we should get as many results as input data
        assert len(result['map_computation_result']) == nb_data

        # verifying that every expected result is returned
        for i in range(nb_data):
            data = data_array[i]
            assert [{'computation_result': data * mult}] in result['map_computation_result']
        

    @mock.patch('outflow.core.db.database.Databases.session', new_callable=mock.PropertyMock)
    def test_compute_more_with_results_not_in_base(self, mock_session):
        """
        Testing when some data are not already in database
        Request to DB returns a ComputationResult or a NoResultFound exception
        """

        # --- define test data ---
        nb_data = randrange(2,5)
        nb_data_not_in_db = randrange(1,nb_data)
        data_not_in_db = sample(range(nb_data), k=nb_data_not_in_db)
        data_array = sample(range(100), k=nb_data)
        mult = randrange(10)
        db_result_list = []

        # --- define the data returned by the request to the DB ---
        from sqlalchemy.orm.exc import NoResultFound
        for i in range(nb_data):
            if i in data_not_in_db:
                db_result_list.append(NoResultFound)
            else:
                db_result_list.append(ComputationResult(
                    input_value = data_array[i],
                    multiplier = mult,
                    result = data_array[i] * mult
                ))

        # --- define mock session queries
        mock_session\
            .return_value.query\
            .return_value.filter_by\
            .return_value.one.side_effect = db_result_list

        # -- define args
        context.args = Namespace(multiplier = mult, dry_run = False, db_untracked = False)

        # --- initialize the task ---
        with MapTask(output_name="map_computation_result") as mapped_computation:
            compute = ComputeMore()
        self.task = mapped_computation

        # --- run the task ---
        result = self.run_task(some_data_array = data_array)

        # --- make assertions ---

        # filtering "add" calls with a ComputationResult object as parameter
        call_add_computation_result = [
            call.args[0] 
            for call in mock_session.return_value.add.call_args_list 
            if isinstance(call.args[0], ComputationResult)]

        # nb_data_not_in_db should be added since they were not found
        assert len(call_add_computation_result) == nb_data_not_in_db

        # check the type of result
        assert isinstance(result, dict)
        assert 'map_computation_result' in result

        # we should get as many results as input data
        assert len(result['map_computation_result']) == nb_data

        # verifying that every expected result is returned
        for i in range(nb_data):
            data = data_array[i]
            assert [{'computation_result': data * mult}] in result['map_computation_result']
        
        

        