# -*- coding: utf-8 -*-
import pytest
from unittest import mock
import os
import copy
import tempfile
import shutil
from random import randrange

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from outflow.core.test import CommandTestCase
from outflow.core.test.test_cases import (PostgresCommandTestCase,
                                          postgresql_fixture)

from outflow.core.logging import logger
from outflow.core.pipeline import context, config, settings

from tuto.data_reduction.models.computation_result import ComputationResult

class TestDataReductionNoCmd(CommandTestCase):
    def test_data_reduction(self):

        # --- initialize the command ---

        from tuto.data_reduction.commands import DataReduction
        self.root_command_class = DataReduction

        arg_list = []

        # --- run the command ---
        return_code, result = self.run_command(arg_list)

        # --- make assertions ---
        assert return_code == 0
        assert result == [{"None": None}]


class TestDataReductionCmd(PostgresCommandTestCase):
    
    PLUGINS = ['outflow.management', 'tuto.data_reduction']

    # Automatically run a db upgrade heads before each test
    @pytest.fixture(autouse=True)
    def setup_db_upgrade(self, with_pipeline_context_manager):
        # -- commands to be executed
        db_upgrade = ['management', 'db', 'upgrade', 'heads', '-ll', 'INFO']
        self.run_command(db_upgrade, force_dry_run = False)

    # --- test if the upgrade is ok
    def test_db_upgrade(self):
        with self.pipeline_db_session() as session:
            # The table computation_result has be to created
            try:
                c = session.query(ComputationResult).count()
                # and it should be empty
                assert c == 0
            except Exception as e:
                assert False, e

    def test_compute_one(self):

        # --- create some fake data ---
        multiplier = randrange(10)

        # --- prepare command with its arguments ---
        command = [
            'compute_one_data',
            '--multiplier',
            f'{multiplier}',
            '-ll',
            'INFO',
        ]

        # --- run the command ---
        return_code, result = self.run_command(command, force_dry_run = False)

        # --- make assertions ---
        assert return_code == 0
        assert result[0]['computation_result'] == 40 * multiplier


    def test_compute_more(self, caplog):

        # --- create some fake data ---
        multiplier = randrange(10)

        # --- prepare command with its arguments ---
        command = [
            'compute_more_data',
            '--multiplier',
            f'{multiplier}',
            '-ll',
            'INFO'
        ]

        # --- run the command ---
        return_code, result = self.run_command(command, force_dry_run = False)

        # --- make assertions ---
        assert return_code == 0
        for i in range(40, 71, 10):
            res = [{'computation_result': i * multiplier}]
            assert res in result[0]['map_computation_result']


    def test_compute_more_ray(self, caplog):

        # --- setup configuration to use ray backend ---
        custom_config = {
            "backend": "ray",
            "ray": {"cluster_type": "local"}
        }
        config.update(custom_config)

        # --- create some fake data ---
        multiplier = randrange(10)

        # --- prepare command with its arguments ---
        command = [
            'compute_more_data',
            '--multiplier',
            f'{multiplier}',
            '-ll',
            'INFO'
        ]

        # --- run the command ---
        return_code, result = self.run_command(command, force_dry_run = False)

        # --- make assertions ---
        assert return_code == 0
        for i in range(40, 71, 10):
            res = [{'computation_result': i * multiplier}]
            assert res in result[0]['map_computation_result']
