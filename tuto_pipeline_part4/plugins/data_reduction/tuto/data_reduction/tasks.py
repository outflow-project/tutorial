import time
from random import randrange

from outflow.core.logging import logger
from outflow.core.tasks import Task
from outflow.core.pipeline import context, config
from outflow.core.types import IterateOn

from sqlalchemy.orm.exc import NoResultFound

from .models.computation_result import ComputationResult

@Task.as_task
def GenOneData() -> {'some_data': int}:
    some_data = 40
    return {'some_data': some_data}

@Task.as_task
def GenMoreData() -> {'some_data_array': list}:
    some_data_array = [40, 50, 60, 70]
    return {'some_data_array': some_data_array}

def compute(context, some_data):
    # the main context seems to be unknown outside of a Task

    # get the session of the default database
    session = context.session

    # get the multiplier from the cli arguments
    multiplier = context.args.multiplier 
    
    logger.info("some_data = {}".format(some_data))
    logger.info("multiplier = {}".format(multiplier))

    # Check if the result of the computation is already in the database
    try:
        # query the database with our model ComputationResult
        computation_result_obj = session.query(ComputationResult) \
            .filter_by(input_value=some_data, multiplier=multiplier).one()

        logger.info('Result found in database')

        # get the result from the model object (ie from the row in the table)
        computation_result = computation_result_obj.result

    except NoResultFound:
        # Result not in the database: compute the value like before
        logger.info('Result not found in database, computing result and inserting')

        # if 'ray' in config :
        #     # add this line to access number of CPUs available
        #     from ray import get_resource_ids
        #     cpu_count = len(get_resource_ids()["CPU"])

        #     # simulate a multiprocess computation
        #     computation_time = 5/cpu_count
        #     logger.info(f"{cpu_count} CPUs available, computation will last {computation_time} seconds")
        #     time.sleep(computation_time)
        # else:
        #     # simulate a big computation
        #     time.sleep(3)
        # simulate a big computation
        time.sleep(3)

        computation_result = some_data * multiplier
        logger.info(computation_result)

        # create an object ComputationResult
        computation_result_obj = ComputationResult(
            input_value=some_data,
            multiplier=multiplier,
            result=computation_result
        )

        # and insert it in the database
        session.add(computation_result_obj)
        session.commit()
        
    # logger.info(session.mock_calls)
    # logger.info(config["databases"].mock_calls)

    # return the result for the next task
    logger.warning(computation_result)
    return {'computation_result': computation_result}

@Task.as_task
def ComputeOne(some_data: int) -> {'computation_result': int}:
    return compute(context, some_data)

@Task.as_task
def ComputeMore(some_data: IterateOn('some_data_array', int)) -> {'computation_result': int}:
    return compute(context, some_data)

@Task.as_task
def PrintOneData(computation_result: int) -> {'computation_result': int}:
    logger.info(f'Result of the computation: {computation_result}')
    return computation_result

@Task.as_task
def PrintMoreData(map_computation_result: list) -> {'map_computation_result': list}:
    logger.info(f'Result of the mapped computation: {map_computation_result}')
    return map_computation_result