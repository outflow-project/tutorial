from outflow.core.commands import Command, RootCommand
from outflow.core.logging import logger
from outflow.library.tasks import MapTask

from .tasks import GenOneData, PrintOneData, ComputeOne
from .tasks import GenMoreData, PrintMoreData, ComputeMore


@RootCommand.subcommand()
class ComputeOneData(Command):
    """
    Command to compute and print some data
    """

    def add_arguments(self):
        # my arg1
        self.add_argument(
            '--multiplier',
            help="""
            Value by which the generated value will be multiplied
            """,
            type=int,
            required=True
        )

    def setup_tasks(self):

        # instantiate tasks
        gen_data = GenOneData()
        compute = ComputeOne()
        print_data = PrintOneData()

        ## setup the workflow
        gen_data >> compute >> print_data

        # return the terminating task(s)
        return print_data

@RootCommand.subcommand()
class ComputeMoreData(Command):
    """
    Command to compute and print some data
    """

    def add_arguments(self):
        # my arg1
        self.add_argument(
            '--multiplier',
            help="""
            Value by which the generated value will be multiplied
            """,
            type=int,
            required=True
        )

    def setup_tasks(self):

        # instantiate tasks
        gen_data = GenMoreData()
        print_data = PrintMoreData()

        # create and parametrize a MapTask
        with MapTask(output_name="map_computation_result") as mapped_computation:
            compute = ComputeMore()

        ## setup the workflow
        gen_data  >> mapped_computation >> print_data

        # return the terminating task(s)
        return print_data

@RootCommand.subcommand(db_untracked=True)
def DataReduction():
    """
    DataReduction subcommand
    """

    logger.info("Hello from data_reduction")

    return {"ok": "ok"}