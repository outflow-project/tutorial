from outflow.core.logging import logger
from outflow.core.tasks.task import Task

import time


@Task.as_task
def GenData() -> {"some_data": int}:
    some_data = 42
    return {"some_data": some_data}


@Task.as_task
def Compute(some_data: int) -> {"computation_result": int}:
    # Simulate a big computation

    logger.info("Running computation")
    time.sleep(5)
    result = some_data * 2

    # return the result for the next task
    return {"computation_result": result}


@Task.as_task
def PrintData(computation_result: int):
    logger.info(f"Result of the computation: {computation_result}")
