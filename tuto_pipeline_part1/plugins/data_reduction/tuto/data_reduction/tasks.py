from outflow.core.logging import logger
from outflow.core.tasks.task import Task


@Task.as_task
def GenData() -> {"some_data": int}:
    some_data = 42
    return {"some_data": some_data}


@Task.as_task
def PrintData(some_data: int):
    logger.info(f"Result of the computation: {some_data}")