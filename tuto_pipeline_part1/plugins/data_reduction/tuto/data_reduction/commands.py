from outflow.core.command import Command, RootCommand
from outflow.core.logging import logger

from .tasks import GenData, PrintData


# @RootCommand.subcommand()
# class ComputeData(Command):
#     """
#     Command to compute and print some data
#     """
#
#     # def add_arguments(self):
#     #     # my arg1
#     #     self.add_argument(
#     #         '--my_arg1',
#     #         help="""
#     #         Arg1 help
#     #         """,
#     #     )
#
#     def setup_tasks(self):
#
#         # instantiate tasks
#         gen_data = GenData()
#         print_data = PrintData()
#
#         # setup the workflow
#         gen_data >> print_data
#
#         # return the terminating task(s)
#         return print_data

@RootCommand.subcommand()
def DataReduction():
    """
    DataReduction subcommand
    """

    logger.info("Hello from data_reduction")